# how to run the demo

- go to the folder where the code is "checkouted"

## build the project & create an "exploded" version of the distribution package
- mvn clean install -Ddistribution=development

## launch karaf with the demo
- .\distribution\target\fr.matoeil.demo.osgi.configuration.distribution-1.0.0-SNAPSHOT\bin\karaf clean debug

## test it
- open a web browser
- go to "http://localhost:1024/"
- the page content is an "echo" of the request send to the server

## change the configuration
- open a web browser
- go to http://localhost:8080/system/console/configMgr
- click on the "Ping Service Configuration" section
- change the port value to 1025 for example
- click on save

## re test it
- open a web browser
- go to "http://localhost:1025/"
- the page content is an "echo" of the request send to the server
- if you go to "http://localhost:1024/", there is no more reply from the server

## change the configuration again
- open a web browser
- go to http://localhost:8080/system/console/configMgr
- click on the "Ping Service Configuration" section
- change the port value to 1025 for example
- click on save

## re re test it
- open a web browser
- go to "http://localhost:1024/"
- the page content is an "echo" of the request send to the server
- if you go to "http://localhost:1025/", there is no more reply from the server

## voila

# under the hood

- In the ping module, the package fr.matoeil.demo.osgi.configuration.ping.internal contains :
    - the ping service implemented with the help of netty (NettyPingService.java)
    - the ping service configuration represented by PingServiceConfiguration.java interface
    - the PingServiceConfiguration interface is annotated in order to produce "metatype" information
    - the metatype information file is generated with the help of the maven-bundle-plugin (see osgi.bnd file, the -metatype: ${bundle.namespace}.internal.* part)

- In the ping module, the package fr.matoeil.demo.osgi.configuration.ping.internal.activation contains :
    - the PingManagedService.java class. This class is registered as a osgi service on the bundle start and will be notified when a configuration property under
        the key "fr.matoeil.demo.osgi.configuration.ping" is modified. A PingServiceConfiguration instance will be created with the help of Configurable.createConfigurable method and this instance will be passed to the
        NettyPingService. When the NettyPingService receive a new PingServiceConfiguration instance, it will reconfigure itself and change the opened port according to the value set in the PingServiceConfiguration instance
    - the BundleModule.java is a guice Module defining the wiring of the different instances of the bundle. It define the creation
        of the NettyPingService, register the PingManagedService is the osgi registry along to the pid corresponding to the configuration key.
    - The Activator.java is the class responsible of the bootstrap of the bundle. It will be notified of the start and the stop of the bundle.
        When starting, it will use the guice injector along to the BundleModule in order to create the bundle's objects.
        In the osgi.bnd file, the "Bundle-Activator" section indicate the osgi runtime which class to use to notify the state of the bundle.

- The "features" module contains a features.xml files describing the different osgi bundle to deploy in the osgi (karaf) runtime

- The "distribution" module contains files describing how to generate the distribution archive. Two profiles are supported:
    - one generate a tar.gz and zip files. Only decompress a distribution archive, launch the script "./bin/karaf" in order to launch the demo.
        Use the maven command "mvn clean install -Ddistribution=release" to generate it.
    - one generate a exploded version of the distribution archive into the folder "distribution\target\fr.matoeil.demo.osgi.configuration.distribution-1.0.0-SNAPSHOT".
        Only launch the script "./bin/karaf" in this folder in order to launch the demo
        Use the maven command "mvn clean install -Ddistribution=development" to generate it.
