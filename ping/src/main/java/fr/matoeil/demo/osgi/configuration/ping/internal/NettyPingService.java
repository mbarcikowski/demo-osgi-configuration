/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.demo.osgi.configuration.ping.internal;

import org.jboss.netty.bootstrap.ServerBootstrap;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelFuture;
import org.jboss.netty.channel.ChannelPipeline;
import org.jboss.netty.channel.ChannelPipelineFactory;
import org.jboss.netty.channel.Channels;
import org.jboss.netty.channel.socket.nio.NioServerSocketChannelFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;
import javax.inject.Inject;
import java.net.InetSocketAddress;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * @author mathieu.barcikowski@misys.com
 */
public class NettyPingService implements PingService {
    private static final Logger LOGGER = LoggerFactory.getLogger(NettyPingService.class);
    private static final int TIMEOUT = 2;

    private PingServiceConfiguration configuration_;
    private NioServerSocketChannelFactory channelFactory_;
    @Nullable
    private Channel channel_ = null;
    private ServerBootstrap bootstrap_;

    @Override
    @Inject
    public final void setConfiguration(final PingServiceConfiguration aConfiguration) {
        LOGGER.debug("NettyPingService setConfiguration with port {}", aConfiguration.port());
        configuration_ = aConfiguration;
        if (channel_ != null) {
            ChannelFuture channelFuture = channel_.unbind();
            channelFuture.awaitUninterruptibly(TIMEOUT, TimeUnit.SECONDS);
            bind();
        }
    }

    @Override
    public final void start() {
        LOGGER.debug("NettyPingService start with port {}", configuration_.port());
        channelFactory_ = new NioServerSocketChannelFactory(
                Executors.newCachedThreadPool(),
                Executors.newCachedThreadPool());
        bootstrap_ = new ServerBootstrap(
                channelFactory_);

        // Configure the pipeline factory.
        bootstrap_.setPipelineFactory(new ChannelPipelineFactory() {
            public ChannelPipeline getPipeline() {
                return Channels.pipeline(new EchoServerHandler());
            }
        });
        bind();


    }

    private void bind() {
        // Bind and start to accept incoming connections.
        channel_ = bootstrap_.bind(new InetSocketAddress(configuration_.port()));
    }

    @Override
    public final void stop() {
        LOGGER.debug("NettyPingService stop");
        ChannelFuture future = channel_.close();
        future.awaitUninterruptibly(TIMEOUT, TimeUnit.SECONDS);
        channelFactory_.releaseExternalResources();
    }
}
