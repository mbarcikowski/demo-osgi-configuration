/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.demo.osgi.configuration.ping.internal.activation;

import com.google.inject.AbstractModule;
import fr.matoeil.demo.osgi.configuration.ping.internal.DefaultPingServiceConfiguration;
import fr.matoeil.demo.osgi.configuration.ping.internal.NettyPingService;
import fr.matoeil.demo.osgi.configuration.ping.internal.PingService;
import fr.matoeil.demo.osgi.configuration.ping.internal.PingServiceConfiguration;
import org.osgi.service.cm.ManagedService;

import java.util.HashMap;
import java.util.Map;

import static org.ops4j.peaberry.Peaberry.service;
import static org.ops4j.peaberry.util.TypeLiterals.export;

/**
 * @author mathieu.barcikowski@misys.com
 */
public class BundleModule extends AbstractModule {
    @Override
    protected void configure() {

        bind(DefaultPingServiceConfiguration.class).asEagerSingleton();
        bind(PingServiceConfiguration.class).to(DefaultPingServiceConfiguration.class).asEagerSingleton();
        bind(PingManagedService.class).asEagerSingleton();
        bind(NettyPingService.class).asEagerSingleton();
        bind(PingService.class).to(NettyPingService.class).asEagerSingleton();

        Map<String, Object> attributes = new HashMap<>();
        attributes.put("service.pid", "fr.matoeil.demo.osgi.configuration.ping");
        bind(export(ManagedService.class)).toProvider(service(PingManagedService.class).attributes(attributes).export()).asEagerSingleton();
    }
}
