/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.demo.osgi.configuration.ping.internal.activation;

import aQute.bnd.annotation.metatype.Configurable;
import fr.matoeil.demo.osgi.configuration.ping.internal.DefaultPingServiceConfiguration;
import fr.matoeil.demo.osgi.configuration.ping.internal.PingService;
import fr.matoeil.demo.osgi.configuration.ping.internal.PingServiceConfiguration;
import org.osgi.service.cm.ManagedService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.util.Dictionary;

/**
 * @author mathieu.barcikowski@misys.com
 */
public class PingManagedService implements ManagedService {

    private static final Logger LOGGER = LoggerFactory.getLogger(PingManagedService.class);

    private final DefaultPingServiceConfiguration defaultConfiguration_;
    private PingService pingService_;

    @Inject
    public PingManagedService(final DefaultPingServiceConfiguration aDefaultPingServiceConfiguration) {
        defaultConfiguration_ = aDefaultPingServiceConfiguration;
    }

    @Inject
    public final void setPingService(final PingService aPingService) {
        pingService_ = aPingService;
    }

    @Override
    public final void updated(final Dictionary<String, ?> aAttributes) {
        if (aAttributes == null) {
            LOGGER.debug("update Ping Service Configuration with default configuration");
            pingService_.setConfiguration(defaultConfiguration_);
        } else {
            LOGGER.debug("update Ping Service Configuration with new configuration");
            PingServiceConfiguration configuration = Configurable.createConfigurable(PingServiceConfiguration.class, aAttributes);
            pingService_.setConfiguration(configuration);
        }

    }
}
