/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.demo.osgi.configuration.ping.internal;

import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.channel.ChannelFuture;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.channel.ExceptionEvent;
import org.jboss.netty.channel.MessageEvent;
import org.jboss.netty.channel.SimpleChannelUpstreamHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;

/**
 * @author mathieu.barcikowski@misys.com
 */
public class EchoServerHandler extends SimpleChannelUpstreamHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(EchoServerHandler.class);
    private static final int TIMEOUT = 2;

    private final AtomicLong transferredBytes = new AtomicLong();

    public final long getTransferredBytes() {
        return transferredBytes.get();
    }

    @Override
    public final void messageReceived(
            ChannelHandlerContext ctx, MessageEvent e) {
        // Send back the received message to the remote peer.
        transferredBytes.addAndGet(((ChannelBuffer) e.getMessage()).readableBytes());
        ChannelFuture channelFuture = e.getChannel().write(e.getMessage());
        channelFuture.awaitUninterruptibly(TIMEOUT, TimeUnit.SECONDS);
        e.getChannel().close();
    }

    @Override
    public final void exceptionCaught(
            ChannelHandlerContext ctx, ExceptionEvent e) {
        // Close the connection when an exception is raised.
        LOGGER.warn("Unexpected exception from downstream.", e.getCause());
        e.getChannel().close();
    }
}

